angular.module('FHEditor')
.factory('Item', function(Product){

  return function(id, file){
    this.width = Product.getWidth();
    this.height = Product.getHeight();
    this.id = id;
    this.file = file;
    this.qnt = 1;
    this.uploadProgress = 0;
    var self = this;
    this.setQnt = function(value){
      if (!value || +value < 1) {
        this.qnt = 1;
        return;
      }
      this.qnt = +value;
    };
    this.serialize = function(){
      return {
        id: self.id,
        qnt: self.qnt
      }
    }
  }

});