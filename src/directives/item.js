angular.module('FHEditor')
.directive('item', function(){
  return {
    restrict: 'E',
    templateUrl: 'src/templates/item.html',
    controllerAs: 'itemCtrl',
    scope: { id: '=' },
    controller: function(Items, $timeout, Product, $scope){
      var self = this;
      var reader = new FileReader();
      this.width = Product.getWidth();
      this.height = Product.getHeight();
      this.mask = Product.getMask();
      this.item = Items.getItems()[$scope.id];
      var f = this.item.file;
      this.fileName = f.name;
      this.thumb = null;
      this.incQnt = function(){
        this.item.setQnt(this.item.qnt + 1);
      };
      this.decQnt = function(){
        this.item.setQnt(this.item.qnt - 1);
      };
      reader.onload = function(e){
        //todo: remove $timeout
        self.thumb = e.target.result;
        $timeout(function(){});
      };
      reader.readAsDataURL(f);
    }
  }
});
