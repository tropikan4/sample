angular.module('FHEditor')
.service('Items', function(Item, $timeout){
  var _items = [];

  this.addItem = function(item){
    //todo: research how to fire without $timeout
    _items.push(item);
    $timeout(function(){});
  };

  this.getItems = function(){
    return _items;
  };

  this.serialize = function(){
    result = [];
    angular.forEach(_items, function(item){
      result.push(item.serialize());
    });
    return result;
  };


});