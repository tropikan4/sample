angular.module('FHEditor')
.directive('user', function(){
  return {
    restrict: 'E',
    templateUrl: 'src/templates/user.html',
    controllerAs: 'userCtrl',
    controller: function($state, User, $location, Product, Order){
      var productType = +$location.search()['type'];
      if (productType) Product.setType(productType);

      this.fio = User.getFIO();
      this.phone = User.getPhone();
      this.email = User.getEmail();

      this.nextStep = function(){
        User.setFIO(this.fio);
        User.setPhone(this.phone);
        User.setEmail(this.email);
        Order.init().then(function(){$state.go('subtypes');});
      }
    }
  }
});