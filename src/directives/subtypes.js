angular.module('FHEditor')
.directive('subtypes', function(){
  return {
    restrict: 'E',
    templateUrl: 'src/templates/subtypes.html',
    controllerAs: 'subtypesCtrl',
    controller: function($http, $state, Product){
      var self = this;

      this.subtypes = [];

      this.nextStep = function(subtype){
        Product.setSubType(subtype.id);
        $http.get('api/item.php?id=' + subtype.id).then(function(response){
          var data = response.data;
          Product.setWidth(data['width']);
          Product.setHeight(data['height']);
          Product.setMask(data['mask']);
          $state.go('items');
        });
      };

      $http.get('api/subtypes.php?id=' + Product.getType() ).then(function(response){
        self.subtypes = response.data;
      });
    }
  }
});