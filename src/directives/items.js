angular.module('FHEditor')
.directive('items', function(){
  return {
    restrict: 'E',
    templateUrl: 'src/templates/items.html',
    controllerAs: 'itemsCtrl',
    scope: {},
    controller: function(Items, $state){
      this.items = Items.getItems();

      this.uploadFiles = function(){
        $state.go('uploadFiles');
      };

      this.allX = function(qnt){
        angular.forEach(this.items, function(item){
          item.setQnt(qnt);
        });
      };
    }
  }
});
