var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var templateCache = require('gulp-angular-templatecache');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');

gulp.task('serve', function(){
  browserSync.init({
    server: {
      baseDir: './'
    }
  });
  gulp.watch('./index.html').on('change', browserSync.reload);
});

gulp.task('cache', function () {
  return gulp.src('./src/templates/*.html')
    .pipe(templateCache({
      module: 'FHEditor'
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('concat', function(){
  return gulp.src(['./src/**/*.js', './dist/templates.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist'))
});

gulp.task('annotate', function () {
  return gulp.src('./dist/app.js')
    .pipe(ngAnnotate())
    .pipe(gulp.dest('dist'));
});

gulp.task('compress', function() {
  return gulp.src('dist/app.js')
    .pipe(uglify())
    .pipe(gulp.dest('.'));
});
