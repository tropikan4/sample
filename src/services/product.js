angular.module('FHEditor')
.service('Product', function(){
  var _width = 4, _height = 4;
  var _mask = undefined;
  var _rel = undefined;
  var _type = undefined;
  var _subtype = undefined;

  this.getWidth = function(){
    return _width;
  };

  this.getHeight = function(){
    return _height;
  };

  this.getRel = function(){
    if (_rel) return _rel;
    var result = (_width > _height) ? _height / _width : _width / _height;
    _rel = result;
    return result;
  };

  this.getMask = function(){
    return _mask;
  };

  this.getType = function(){
    return _type;
  };

  this.setType = function(value){
    _type = value;
  };

  this.getSubType = function(){
    return _subtype;
  };

  this.setSubType = function(value){
    _subtype = value;
  };

  this.setWidth = function(value){
    _width = +value;
  };

  this.setHeight = function(value){
    _height = +value;
  };

  this.setMask = function(value){
    _mask = value;
  };
});