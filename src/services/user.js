angular.module('FHEditor')
.service('User', function(){
  var _fio = localStorage.getItem('fio') || '';
  var _phone = localStorage.getItem('phone') || '';
  var _email = localStorage.getItem('email') || '';

  this.getFIO = function(){
    return _fio;
  };

  this.getPhone = function(){
    return _phone;
  };

  this.getEmail = function(){
    return _email;
  };

  this.setFIO = function(value){
    _fio = value;
    localStorage.setItem('fio', value);
  };

  this.setPhone = function(value){
    _phone = value;
    localStorage.setItem('phone', value);
  };

  this.setEmail = function(value){
    _email = value;
    localStorage.setItem('email', value);
  };
});