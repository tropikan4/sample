angular.module('FHEditor')
.directive('fileSelect', function(){
  return {
    restrict: 'E',
    templateUrl: 'src/templates/file_select.html',
    controllerAs: 'fileSelectCtrl',
    controller: function(){
      this.selectFiles = function(){
        document.getElementById('input-file').click();
      }
    }
  }
});
