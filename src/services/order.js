angular.module('FHEditor')
.service('Order', function($http, User, $httpParamSerializerJQLike, Items, Product, Delivery){
  var _number;
  var self = this;
  var _comments = '';

  this.init = function(){
    return $http({
      method: 'POST',
      url: 'api/init_order.php',
      data: $httpParamSerializerJQLike({
        fio: User.getFIO(),
        phone: User.getPhone(),
        email: User.getEmail()
      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function(response){
      _number = response.data['number'];
    });
  };

  this.serialize = function(){
    return {
      order_number: _number,
      type: Product.getType(),
      subtype: Product.getSubType(),
      comments: _comments
    }
  };

  this.complete = function(){
    return $http({
      method: 'POST',
      url: 'api/complete_order.php',
      data: $httpParamSerializerJQLike({
        order: self.serialize(),
        items: Items.serialize(),
        delivery: Delivery.serialize()
      }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    });
  };

  this.getNumber = function(){
    return _number;
  };

  this.getComments = function(){
    return _comments;
  };

  this.setComments = function(value){
    _comments = value;
  };
});