angular.module('FHEditor')
.service('Delivery', function(){
  var _needDelivery = false;
  var _address, _date;
  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var _timeFrom = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 9, 0, 0, 0);
  var _timeTill = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 18, 0, 0, 0);

  this.serialize = function(){
    if (_needDelivery){
      return {
        need_delivery: true,
        address: _address,
        date: _date,
        time_from: _timeFrom,
        time_till: _timeTill
      }
    } else {
      return {
        need_delivery: false
      }
    }
  };

  this.getTimeFrom = function(){
    return _timeFrom;
  };

  this.getTimeTill = function(){
    return _timeTill;
  };

  this.setNeedDelivery = function(value){
    _needDelivery = value;
  };

  this.setDate = function(value){
    _date = value;
  };

  this.setAddress = function(value){
    _address = value;
  };

  this.setTimeFrom = function(value){
    _timeFrom = value;
  };

  this.setTimeTill = function(value){
    _timeTill = value;
  };
});