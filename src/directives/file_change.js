angular.module('FHEditor')
.directive('fileChange', function(Items, Item){
  return {
    restrict: 'A',
    link: function(scope, element){
      element.bind('change', function(event){
        var files = event.target.files;
        var l = files.length;
        for (var i=0; i < l; i++){
          var id = Items.getItems().length;
          //console.log(files[i]);
          var item = new Item(id, files[i]);
          Items.addItem(item);
        }
      });
    }
  }
});