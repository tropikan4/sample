angular.module('FHEditor')
.directive('uploadFiles', function(){
  return {
    restrict: 'E',
    templateUrl: 'src/templates/upload_files.html',
    controllerAs: 'uploadFilesCtrl',
    controller: function(Items, $timeout, $state, Order){
      this.items = Items.getItems();

      var self = this;
      var uploader = new uploaderObject({});
      var i = 0;



      var processQueue = function(){
        if ( i >= self.items.length ) {
          $state.go('delivery');
          return;
        }
        var item = self.items[i];
        uploader.init({
          file:         item.file,
          order_number: Order.getNumber(),
          url:          './serverLogic.php',
          fieldName:    'photo',
          newFileName:   i + '.jpg',
          onprogress: function(percents) {
            item.uploadProgress = percents;
          },
          oncomplete: function( done, data ) {
            if(done) {
              item.uploadProgress = 100;
            } else {
              console.log('upload false: ' + i + '.jpg');
            }
            i++;
            $timeout(processQueue, 200);
          }
        });
        uploader.start();
      };

      processQueue();
    }
  }
});