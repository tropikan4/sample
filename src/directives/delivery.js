angular.module('FHEditor')
.directive('delivery', function(){
  return {
    restrict: 'E',
    templateUrl: 'src/templates/delivery.html',
    controllerAs: 'deliveryCtrl',
    controller: function($uibModal, $http, $httpParamSerializerJQLike, Order, Delivery){
      var tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      this.maxTime = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 18, 0, 0, 0);
      this.minTime = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 9, 0, 0, 0);
      this.timeFrom = Delivery.getTimeFrom();
      this.timeTill = Delivery.getTimeTill();
      this.comments = Order.getComments();

      var completeOrderMessage = function (size) {
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'src/templates/confirm_order.html',
          controllerAs: 'modalCompleteOrderCtrl',
          controller: function(){
            this.ok = function(){
              if (window.location.host === 'localhost'){
                window.location = 'http://localhost/fotohunterplus.ru';
              } else {
                window.location = 'http://fotohunterplus.ru';
              }
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          console.log('resolved');
          //$scope.selected = selectedItem;
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });
      };

      var self = this;
      this.needDelivery = undefined;
      this.price = 250;
      this.completeOrder = function(){
        Delivery.setNeedDelivery(this.needDelivery);
        Order.setComments(this.comments);
        if (this.needDelivery){
          Delivery.setAddress(this.address);
          Delivery.setDate(this.date);
          Delivery.setTimeFrom(this.timeFrom);
          Delivery.setTimeTill(this.timeTill);
        }
        Order.complete().then(function(response){
          //console.log(response.data);
          completeOrderMessage();
        });
      };

      this.checkTime = function(){
        if (this.timeFrom >= this.timeTill){
          this.timeFrom = Delivery.getTimeFrom();
          this.timeTill = Delivery.getTimeTill();
        } else {
          Delivery.setTimeFrom(this.timeFrom);
          Delivery.setTimeTill(this.timeTill);
        }
      }
    }
  }
});